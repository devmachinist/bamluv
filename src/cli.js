#!usr/env node
import minimist from 'minimist';
import { login } from '../bin/user';
import { rodeo } from '../bin/rodeo';
import { help } from '../bin/help';

import { version } from '../bin/version';
import { writeserver } from '../create'

 host = require('host')

export async function cli(argsArray) {
  const args = minimist(argsArray.slice(2));
  let cmd = args._[0] || 'help';
  let opt = args._[1];
  
  host.name = () => (args.length > 2 )? args._[args.length] : host.name;
  if (args.version || args.v) {
    cmd = 'version';
  }

  if (args.help || args.h) {
    cmd = 'help';
  }

  switch (cmd) {
    case 'version':
      version(args);
      break;

    case 'help':
      help(args);
      break;

    case 'now':
      now(args);
      break;

    case 'forecast':
      forecast(args);
      break;

    case 'config':
      configure(args);
      break;

    default:
      console.error(`"${cmd}" is not a valid command!`);
      break;
  }
}